package com.allstate.service.interaction;

import com.allstate.models.interaction.Interaction;
import com.allstate.models.interaction.InteractionStatus;
import com.allstate.repository.interaction.InteractionRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InteractionServiceImplTest {

    @Autowired
    InteractionService service;

    @Autowired
    MongoTemplate tpl;

    private Interaction interaction;

    private String custName = "TestCustName";
    private String userId="TempUser";
    private String subject="TestSubject";
    private String creationDate="01/01/2020";
    private String closeDate="01/02/2020";
    private InteractionStatus status=InteractionStatus.Open;
    private  String comments="TestComments";

    @BeforeEach
    void setUp() {

        interaction = new Interaction(custName, userId, subject, creationDate,
                closeDate, status, comments);
        service.save(interaction);
    }

    @AfterEach
    void tearDown() {
        tpl.remove(interaction);
    }

    @Test
    void saveInteraction() {
        assertEquals(interaction.toString(), service.getUserInteractions(userId).get(0).toString());
    }

    @Test
    void getAllInteractions() {
        Assertions.assertTrue(service.getAllInteractions().size()>0, "No Records returned from DB");
    }

    @Test
    void getUserInteractions() {
        Assertions.assertTrue(service.getUserInteractions(userId).size()>0, "No Records returned from DB");

    }

    @Test
    void deleteInteraction() {
        Interaction intTemp = service.getUserInteractions(userId).get(0);
        service.deleteInteraction(intTemp.getId());
        assertTrue(tpl.findById(intTemp.getId(),Interaction.class)==null);
    }

    @Test
    void updateInteraction() {
        Interaction intTemp = service.getUserInteractions(userId).get(0);
        intTemp.setStatus(InteractionStatus.Close);
        service.updateInteraction(intTemp);
        Assertions.assertEquals(intTemp.toString(), tpl.findById(intTemp.getId(),Interaction.class).toString());


    }


}