package com.allstate.service.user;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.payload.auth.response.MessageResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceImplITest {
    @Autowired
    UserService userService;

    @Autowired
    MongoTemplate mongoTemplate;

    private SignupRequest signUpRequest;

    @BeforeEach
    void setUp(){
        signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setEmail("testeruser123@gmail.com");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");

    }

    @Test
    public void testGetAllUsers(){
       List<User> userList =  userService.getAllUsers();
       assertNotNull(userList);
       assertNotNull(userList.size());
    }

    @Test
    public void testRegister(){
        ResponseEntity<?> response = userService.register(signUpRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @AfterAll
    public  void cleanUp(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query,User.class);
    }

}
