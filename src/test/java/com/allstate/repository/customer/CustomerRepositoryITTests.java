package com.allstate.repository.customer;

import com.allstate.models.common.Address;
import com.allstate.models.common.Name;
import com.allstate.models.customer.Customer;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(properties={"spring.data.mongodb.database=CEMESystemDBTests","spring.data.mongodb.host=127.0.0.1"})
public class CustomerRepositoryITTests {

    @Autowired
    private MongoTemplate template;

    @Autowired
    private CustomerRepository customerRepository;

    private Customer customer;

    @BeforeAll
    public void setUp() {
        customer = new Customer();
        customer.setId(new ObjectId("5fc4a002568b11865b70e8df"));
        customer.setAddress(new Address());
        customer.setName(new Name());
        customer.setEmail("email@email.com");
        customer.setPhoneNumber("+91 6266156155");
    }

    @Test
    @Order(1)
    public void saveCustomer() {
        assertEquals(customerRepository.saveCustomer(customer), "5fc4a002568b11865b70e8df");
    }

    @Test
    @Order(2)
    public void updateCustomer() {
        assertEquals(customerRepository.updateCustomer(customer), 1);
    }

    @Test
    @Order(3)
    public void getAllCustomers() {
        assertEquals(customerRepository.getAllCustomers().size(), 1);
    }

    @Test
    @Order(4)
    public void findCustomer() {
        assertEquals(customerRepository.findCustomer("5fc4a002568b11865b70e8df").getEmail(), customer.getEmail());
    }

    @AfterAll
    public void cleanUp() {
        template.dropCollection(Customer.class);
    }
}
