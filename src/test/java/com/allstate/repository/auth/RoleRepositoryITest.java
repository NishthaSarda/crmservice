package com.allstate.repository.auth;

import com.allstate.models.auth.ERole;
import com.allstate.models.auth.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class RoleRepositoryITest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void testFindByName(){
        Optional<Role> role = roleRepository.findByName(ERole.ROLE_ADMIN);
        assertNotNull(role);
    }
}
