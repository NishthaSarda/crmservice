package com.allstate.security.service;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.repository.auth.UserRepository;
import com.allstate.service.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserDetailsServiceImplITest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDetailsService userDetailsService;

    private SignupRequest signupRequest;

    @Autowired
    UserService userService;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp(){
        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("testuser123");
        signUpRequest.setFullName("testuser123");
        signUpRequest.setPassword("testuser123");
        signUpRequest.setEmail("testuser123@gmail.com");
        userService.register(signUpRequest);
    }

    @Test
    public void testLoadUserByUsername(){
        UserDetails user = userDetailsService.loadUserByUsername("testuser123");
        assertNotNull(user);
        assertEquals("testuser123",user.getUsername());
    }

    @AfterAll
    public void cleanup(){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is("testuser123"));
        mongoTemplate.findAndRemove(query, User.class);
    }
}
