package com.allstate.models.customer;

import com.allstate.models.common.Address;
import com.allstate.models.common.Name;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerTests {

    private Customer customer;

    @BeforeAll
    void setUp() throws Exception {
        customer = new Customer();
        customer.setId(new ObjectId("5fc4a002568b11865b70e8df"));
        customer.setAddress(new Address() {{
            setBuildingNumberAndName("2, Wok tower");
        }});
        customer.setName(new Name() {{
            setLastName("Smith");
        }});
        customer.setEmail("email@email.com");
        customer.setPhoneNumber("+91 6266156155");
    }

    @Test
    void getTests() {
        assertEquals(new ObjectId("5fc4a002568b11865b70e8df"), customer.getId());
        assertEquals("2, Wok tower", customer.getAddress().getBuildingNumberAndName());
        assertEquals("Smith", customer.getName().getLastName());
        assertEquals("email@email.com", customer.getEmail());
        assertEquals("+91 6266156155", customer.getPhoneNumber());
    }
}
