package com.allstate.models.interaction;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InteractionTest {

    Interaction  interaction;
    private String custName = "TestCustName";
    private String userId="TestUserID";
    private String subject="TestSubject";
    private String creationDate="01/01/2020";
    private String closeDate="01/02/2020";
    private InteractionStatus status=InteractionStatus.Open;
    private  String comments="TestComments";

    @BeforeEach
    void setUp() {
        interaction = new Interaction(custName, userId, subject, creationDate,
                closeDate, status, comments);
    }

    @AfterEach
    void tearDown() {
        interaction = null;
    }


    @Test
    void getCustName() {
        assertEquals(custName,interaction.getCustName());
    }

    @Test
    void getUserId() {
        assertEquals(custName,interaction.getCustName());
    }

    @Test
    void setCustName() {
        interaction.setCustName("NewValue");
        assertEquals("NewValue",interaction.getCustName());
    }



    @Test
    void getSubject() {
        assertEquals(subject,interaction.getSubject());
    }

    @Test
    void setSubject() {
        interaction.setSubject("NewValue");
        assertEquals("NewValue",interaction.getSubject());
    }

    @Test
    void getCreationDate() {
        assertEquals(creationDate,interaction.getCreationDate());
    }

    @Test
    void setCreationDate() {
        interaction.setCreationDate("01/01/2010");
        assertEquals("01/01/2010",interaction.getCreationDate());

    }

    @Test
    void getCloseDate() {
        assertEquals(closeDate,interaction.getCloseDate());
    }

    @Test
    void setCloseDate() {
        interaction.setCloseDate("01/02/2010");
        assertEquals("01/02/2010",interaction.getCloseDate());
    }

    @Test
    void getStatus() {
        assertEquals(status,interaction.getStatus());
    }

    @Test
    void setStatus() {
        interaction.setStatus(InteractionStatus.Close);
        assertEquals(InteractionStatus.Close,interaction.getStatus());
    }

    @Test
    void getComments() {
        assertEquals(comments,interaction.getComments());
    }

    @Test
    void setComments() {
        interaction.setComments("NewValue");
        assertEquals("NewValue",interaction.getComments());

    }
}