package com.allstate.models.common;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddressTests {

    private Address address;

    @BeforeAll
    void setUp() throws Exception {
        address = new Address();
        address.setBuildingNumberAndName("2, Wok Tower");
        address.setStreetName("First Street");
        address.setLocality("Downtown");
        address.setCity("New York");
        address.setState("New York");
        address.setZipCode("456456");
    }

    @Test
    void getTests() {
        assertEquals("2, Wok Tower", address.getBuildingNumberAndName());
        assertEquals("First Street", address.getStreetName());
        assertEquals("Downtown", address.getLocality());
        assertEquals("New York", address.getCity());
        assertEquals("New York", address.getState());
        assertEquals("456456", address.getZipCode());
    }
}
