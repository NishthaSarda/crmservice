package com.allstate.controllers.customer;

import com.allstate.exception.CustomerException;
import com.allstate.models.customer.Customer;
import com.allstate.service.customer.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerControllerMockTests {

    @Mock
    private CustomerService customerServiceMock;

    @InjectMocks
    private CustomerController customerController = new CustomerController();

    @Test
    public void getAllCustomersError() {
        when(customerServiceMock.getAllCustomers())
                .thenThrow(new CustomerException("Operation failed!"));
        ResponseEntity<?> response = customerController.getAllCustomers();
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void findCustomerError() {
        when(customerServiceMock.findCustomer(anyString()))
                .thenThrow(new CustomerException("Operation failed!"));
        ResponseEntity<?> response = customerController.findCustomer("5fc4a002568b11865b70e8df");
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void saveCustomerError() {
        when(customerServiceMock.saveCustomer(any()))
                .thenThrow(new CustomerException("Operation failed!"));
        ResponseEntity<?> response = customerController.saveCustomer(new Customer());
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void updateCustomerError() {
        when(customerServiceMock.updateCustomer(any()))
                .thenThrow(new CustomerException("Operation failed!"));
        ResponseEntity<?> response = customerController.updateCustomer(new Customer());
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
