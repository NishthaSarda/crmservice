package com.allstate.controllers.interaction;


import com.allstate.models.auth.ERole;
import com.allstate.models.auth.Role;
import com.allstate.models.auth.User;
import com.allstate.models.customer.Customer;
import com.allstate.models.interaction.Interaction;
import com.allstate.models.interaction.InteractionStatus;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.payload.auth.response.JwtResponse;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(properties={"spring.data.mongodb.database=CEMESystemDBTests","spring.data.mongodb.host=127.0.0.1"})
public class InteractionControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    MongoTemplate template;

    @Autowired
    private TestRestTemplate restTemplate;

    private String basePath = "/api/v1/interaction";

    private Role role;
    private SignupRequest signupRequest;
    private String authToken;
    private HttpHeaders headers;

    private Interaction interaction = new Interaction("TestName","testID","subject",
            "01-01-2020", "01-01-2020", InteractionStatus.Close,"comments");
    @BeforeAll
    public void signUpUser() {
        role = new Role();
        role.setId("5fc4a002568b11865b70e8df");
        role.setName(ERole.ROLE_ADMIN);
        template.save(role);

        signupRequest = new SignupRequest();
        signupRequest.setEmail("admin@gmail.com");
        signupRequest.setFullName("Admin Test");
        signupRequest.setPassword("admin12345");
        signupRequest.setUsername("admin");
        signupRequest.setRole(new HashSet<String>(Arrays.asList("admin")));
        this.restTemplate.postForObject("http://localhost:" + port + "/api/v1/user/signup",
                signupRequest, Object.class);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(signupRequest.getUsername());
        ;
        loginRequest.setPassword(signupRequest.getPassword());
        JwtResponse jwtResponse = this.restTemplate.postForObject("http://localhost:" + port + "/api/v1/user/signin",
                loginRequest, JwtResponse.class);
        authToken = "Bearer " + jwtResponse.getAccessToken();

        headers = new HttpHeaders();
        headers.add("authorization", authToken);
    }

    @Test
    @Order(1)
    public void saveInteraction(){
        HttpEntity<Interaction> interactionEntity = new HttpEntity(interaction, headers);
        ResponseEntity<String> id = this.restTemplate.postForEntity("http://localhost:" + port + basePath + "/save",
                interactionEntity, String.class);
    }
    @Test
    @Order(2)
    public void updateInteraction(){
        HttpEntity<Interaction> interactionEntity = new HttpEntity(interaction, headers);
        ResponseEntity<String> id = this.restTemplate.postForEntity("http://localhost:" + port + basePath + "/update",
                interactionEntity, String.class);
    }
    @Test
    @Order(3)
    public void getAllInteractionByUser() throws Exception {
        HttpEntity<Interaction> httpEntity = new HttpEntity(headers);
        ResponseEntity<? extends ArrayList> response
                = this.restTemplate.exchange("http://localhost:" + port + basePath + "/getbyuser/" + interaction.getUserId(),
                HttpMethod.GET, httpEntity, new ArrayList<Interaction>().getClass());
        assertThat(response.getBody().toArray()).isNotEmpty();
    }
    @Test
    @Order(4)
    public void getAllInteraction()  {
        HttpEntity<Interaction> httpEntity = new HttpEntity(headers);
        ResponseEntity<? extends ArrayList> response
                = this.restTemplate.exchange("http://localhost:" + port + basePath + "/getall",
                HttpMethod.GET, httpEntity, new ArrayList<Interaction>().getClass());
        assertThat(response.getBody().toArray()).isNotEmpty();
    }
    @Test
    @Order(4)
    public void deleteInteraction()  {
        HttpEntity<Interaction> httpEntity = new HttpEntity(headers);
        ResponseEntity<? extends ArrayList> response
                = this.restTemplate.exchange("http://localhost:" + port + basePath + "/delete/" + interaction.getId(),
                HttpMethod.GET, httpEntity, new ArrayList<Interaction>().getClass());
    }

    @AfterAll
    public void cleanup() {
        template.dropCollection(Interaction.class);
        template.dropCollection(User.class);
        template.dropCollection(Role.class);
    }
}
