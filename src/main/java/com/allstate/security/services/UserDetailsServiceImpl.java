package com.allstate.security.services;

import com.allstate.service.user.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.allstate.models.auth.User;
import com.allstate.repository.auth.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final String CLASS_NAME = UserDetailsServiceImpl.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final String METHOD_NAME = "loadUserByUsername()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        LOGGER.info("Exited from "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return UserDetailsImpl.build(user);
    }

}