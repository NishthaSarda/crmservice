package com.allstate.service.interaction;

import com.allstate.exception.InteractionException;
import com.allstate.helper.interaction.InteractionHelper;
import com.allstate.models.interaction.Interaction;
import com.allstate.repository.interaction.InteractionRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InteractionServiceImpl implements InteractionService {

    @Autowired
    InteractionRepository interactionRepo;
    @Override
    public List<Interaction> getAllInteractions() {
        return interactionRepo.getAllInteractions();
    }

    @Override
    public List<Interaction> getUserInteractions(String userId) {
        return interactionRepo.getUserInteractions(userId);
    }

    @Override
    public void save(Interaction interaction) throws InteractionException {
        if (InteractionHelper.validate(interaction)) {
            interactionRepo.saveInteraction(interaction);
        } else {
                throw new InteractionException("Invalid Data");
        }
    }

    @Override
    public void deleteInteraction (String id){
        interactionRepo.deleteInteraction(id);
    }

    @Override
    public void updateInteraction(Interaction interaction) {
        interactionRepo.updateInteraction(interaction);
    }
}
