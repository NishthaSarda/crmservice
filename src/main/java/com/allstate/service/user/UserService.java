package com.allstate.service.user;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.SignupRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    ResponseEntity<?> register(SignupRequest signUpRequest);
    List<User> getAllUsers();
}
