package com.allstate.controllers.customer;

import com.allstate.models.customer.Customer;
import com.allstate.service.customer.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.PrintWriter;
import java.io.StringWriter;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private static final String CLASS_NAME = CustomerController.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<?> getAllCustomers() {
        try {
            LOGGER.info("Method getAllCustomers called in " + CLASS_NAME);
            return ResponseEntity.ok(customerService.getAllCustomers());
        } catch (Exception exception) {
            LOGGER.error("Exception calling getAllCustomers", exception);
            StringWriter stringWriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringWriter));
            return new ResponseEntity<String>(
                    "Error getting all customers.\nException: " + exception.getMessage() + "\nStackTrace: " + stringWriter.toString(),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<?> findCustomer(@PathVariable("id") String id) {
        try {
            LOGGER.info("Method findCustomer called in " + CLASS_NAME);
            return ResponseEntity.ok(customerService.findCustomer(id));
        } catch (Exception exception) {
            LOGGER.error("Exception calling findCustomer", exception);
            StringWriter stringWriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringWriter));
            return new ResponseEntity<String>(
                    "Error finding customer.\nException: " + exception.getMessage() + "\nStackTrace: " + stringWriter.toString(),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<?> saveCustomer(@RequestBody @Validated Customer customer) {
        try {
            LOGGER.info("Method saveCustomer called in " + CLASS_NAME);
            return ResponseEntity.ok(customerService.saveCustomer(customer));
        } catch (Exception exception) {
            LOGGER.error("Exception calling saveCustomer", exception);
            StringWriter stringWriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringWriter));
            return new ResponseEntity<String>(
                    "Error saving customer.\nException: " + exception.getMessage() + "\nStackTrace: " + stringWriter.toString(),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<?> updateCustomer(@RequestBody @Validated Customer customer) {
        try {
            LOGGER.info("Method updateCustomer called in " + CLASS_NAME);
            return ResponseEntity.ok(customerService.updateCustomer(customer));
        } catch (Exception exception) {
            LOGGER.error("Exception calling updateCustomer", exception);
            StringWriter stringWriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringWriter));
            return new ResponseEntity<String>(
                    "Error updating customer.\nException: " + exception.getMessage() + "\nStackTrace: " + stringWriter.toString(),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }
}
