package com.allstate.controllers.user;

import com.allstate.models.auth.User;
import com.allstate.payload.auth.request.LoginRequest;
import com.allstate.payload.auth.request.SignupRequest;
import com.allstate.service.auth.AuthService;
import com.allstate.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private static final String CLASS_NAME = UserController.class.getName();
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    AuthService authService;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public List<User> getAllUsers(){
        final String METHOD_NAME = "getAllUsers()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return userService.getAllUsers();
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        final String METHOD_NAME = "registerUser()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return userService.register(signUpRequest);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        final String METHOD_NAME = "authenticateUser()";
        LOGGER.info("Entered into "+CLASS_NAME+ "  method:"+ METHOD_NAME);
        return  authService.authenticate(loginRequest);
    }
}
