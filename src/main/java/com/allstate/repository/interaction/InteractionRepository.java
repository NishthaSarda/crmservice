package com.allstate.repository.interaction;

import com.allstate.models.interaction.Interaction;
import org.bson.types.ObjectId;

import java.util.List;

public interface InteractionRepository {
    void saveInteraction(Interaction interaction);
    List<Interaction> getAllInteractions();
    List<Interaction> getUserInteractions(String userId);
    void deleteInteraction(String id);
    void updateInteraction(Interaction interaction);
}
