package com.allstate.repository.customer;

import com.allstate.models.customer.Customer;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAllCustomers();

    Customer findCustomer(String id);

    String saveCustomer(Customer customer);

    long updateCustomer(Customer customer);
}
