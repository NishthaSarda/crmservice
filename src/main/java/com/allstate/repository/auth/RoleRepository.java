package com.allstate.repository.auth;

import com.allstate.models.auth.ERole;
import com.allstate.models.auth.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
