package com.allstate.helper.interaction;

import com.allstate.models.interaction.Interaction;

public class InteractionHelper {
    public static boolean validate(Interaction interaction){
       return (interaction!=null && interaction.getCustName()!=null && interaction.getStatus()!=null
         && interaction.getSubject()!=null && interaction.getUserId()!=null);
    }
}
