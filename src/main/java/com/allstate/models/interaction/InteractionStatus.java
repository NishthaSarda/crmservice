package com.allstate.models.interaction;

public enum InteractionStatus {

    Open,
    Close,
    Hold,
    InProgress

}
