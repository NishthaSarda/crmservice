package com.allstate.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidJWTException extends RuntimeException {
    public InvalidJWTException(String message, Exception e) {
        super(message,e);
    }
}
